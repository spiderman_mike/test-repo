const fs = require('fs');

fs.readFile('./package.json', (err, data) => {
    if(process.env.length >= 3) throw new Error('Merged branch argument is required.')
    if(err) throw err;

    // merged branch
    const branch = process.argv[2];
    console.log('current branch: ', branch);

    // Parse Package.json as js Object
    var packageObj = JSON.parse(data);

    // Get version from json object
    var version = packageObj.version;
    console.log('current version: ',version);

    // Split version into three parts Major.Minor.Patch
    let verSplit = version.split('.', 3);

    if(branch.toLocaleLowerCase().includes('release/')) {
        // if Release do Major Version Update
        verSplit[0] = bumpNumber(verSplit[0]);
        verSplit[1] = String(0);
        verSplit[2] = String(0);
    } else if (branch.toLocaleLowerCase().includes('feature/')) {
        // if feature do Minor Version    
        verSplit[1] = bumpNumber(verSplit[1]);
        verSplit[2] = String(0);
    } else if (branch.toLocaleLowerCase().includes('hotfix') || branch.toLowerCase().includes('bugfix')) {
        // if hotfix/bugfix do Patch version
        verSplit[2] = bumpNumber(verSplit[2]);
    }

    // Combine new very nummer.
    const newVersion = verSplit.join('.');
    packageObj.version = newVersion;

    console.log('new version: ',packageObj.version);

    // reformat package.json file
    packageObj = JSON.stringify(packageObj, null, 2);

    fs.writeFile('./package.json', packageObj, (err) => {
        if(err) throw err;
        console.log(`Package.json version has been updated to ${newVersion}`);
    })

});

function bumpNumber(versionArray) {
    const version = versionArray;

    let versionNumber = Number(version); 
    versionNumber++;

    return String(versionNumber);
}

